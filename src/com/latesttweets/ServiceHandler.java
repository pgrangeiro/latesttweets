package com.latesttweets;

import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class ServiceHandler {
	static String response = null;
	
	public ServiceHandler() {
		
	}
	
	public String makeServiceCall(String url) {
		String response = "";
		
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			
			HttpGet httpGet = new HttpGet(url);
			httpResponse = httpClient.execute(httpGet);
			
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
}
