package com.latesttweets;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.latesttweets.R;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		final Button button = (Button) findViewById(R.id.btnSend);
		final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
		
		button.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String urlAPI = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name="+ txtUsername.getText().toString() +"&count=5";
				handleGetTweets(urlAPI);
			}
		});
	}
	
	private void handleGetTweets(String urlAPI) {
		new HttpAsyncTask().execute(urlAPI);
	}
	
	public static String GET(String url) {
		String result = "";
		
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
			
		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}
		
		return result;
	}
	
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			return GET(urls[0]);
		}
		
	}
}
